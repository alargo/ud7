	package Ejercicio3;
	
	import java.util.Enumeration;
	import java.util.Hashtable;
	import java.util.Scanner;
	
	public class Ejercicio3App {
	
		public static void main(String[] args) {	
			//Autor: Alejandro Largo
			
			//con un do while se controla el menu y que nos siga pidiendo opciones hasta que nosotros le demeos a la opcion de salir
			boolean salir =false;
			
			do {
			Scanner eleccion = new Scanner(System.in);
			System.out.println(" -  -  -   ");
			System.out.println("Elige una opcion");
	        System.out.println("-----------------------");
	        System.out.println("1 Mostrar todos los productos ");
	        System.out.println("2 Mostrar Precio");
	        System.out.println("3 A�adir Productos ");
	        System.out.println("4 Buscar un producto");
	        System.out.println("5 Salir");
	        System.out.println();
	        System.out.print("Introducir opcion: ");
			int opcion = eleccion.nextInt();
			
			//se almacena el stock que hay en la tienda
			
			Hashtable<String, Integer> productos = new Hashtable<String, Integer>();
			
			productos.put("camisa", 10);
			productos.put("pantalon corto", 15);
			productos.put("pantalon largo", 20);
			productos.put("camiseta", 5);
			productos.put("Camisa manga corta", 20);
			productos.put("zapatos", 23);
			productos.put("zapatillas", 30);
			productos.put("chanclas", 5);
			productos.put("sudadera", 20);
			productos.put("chaqueta", 40);
			
			
			
			
			switch (opcion) {
			//primeras dos opciones ense�amos los productos y sus precios 
			case 1:
			Enumeration producto = productos.keys();
				while(producto.hasMoreElements())
					  System.out.println(producto.nextElement());
				break;
				
			case 2:	
			Enumeration precio = productos.elements();
				while(precio.hasMoreElements()) {
					System.out.println("Los precios son: " + precio.nextElement());
				}
				break;
				
			case 3:
				//A�adimos un nuevo producto al stock de la tienda 
				// * deberia de funcionar pero no a�ade ningun producto **
			System.out.println("Que producto quieres a�adir ");
				String productoNuevo = eleccion.next();
				
			System.out.println("Que precio tiene el producto ");
				int preciooNuevo = eleccion.nextInt();
				
			if (productos.containsKey(productoNuevo)) {
			    System.out.println("No se puede introducir el producto. El producto esta repetido.");
					} else {
						productos.put(productoNuevo, preciooNuevo);               
							}			
					break;
				
			case 4:
				//se le pasa una condicion para buscar la key del producto y que si es igual a la que se le paso por teclado lo muestre por pantalla
			System.out.println("Que producto quieres buscar ");
				String buscarProducto = eleccion.next();
				
			if (productos.containsKey(buscarProducto)) {
					System.out.println(buscarProducto);
				}else {
					System.out.println("Este producto no esta en stock");
					}			
				break;
				
			case 5:
				salir =true;			
				break;
	
			default:
				break;
					}
				
				}while(!salir);
	
	    	}
		}
		
	
	
