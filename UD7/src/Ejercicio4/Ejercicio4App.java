package Ejercicio4;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

public class Ejercicio4App {

	public static void main(String[] args) {
		//Autor: Alejandro Largo
		
		//En este ejercicio hice funcionar un hasMap para trabajr los productos de la tienda 
		HashMap<String,Float> listaProductos = new HashMap<String,Float>();
	    Scanner eleccion = new Scanner(System.in);
	    int opcionElegida = 0;
	    float precio;
	    String producto;

	    //se crean las variables y el menu para controlar las opcones y que solo se salga y esocgemos la opcion de salir 
	    while (opcionElegida != 4) {
	        System.out.println("Introduce el numero de la opci�n que quieras:");
	        System.out.println("1 Introducir producto");
	        System.out.println("2 Mostrar todos los productos");
	        System.out.println("3 Eliminar producto");
	        System.out.println("4 Salir");
	        opcionElegida = eleccion.nextInt();

	        switch (opcionElegida) {
	            case 1:
	            	//esribimos el producto y llamamos al metodo que nos lo va a almacenar 
	                System.out.println("Introduce el nombre del producto:");
	                producto = eleccion.next();
	                System.out.println("Introduce el precio del producto:");
	                precio = eleccion.nextFloat();
	                almacenarproducto(producto, precio, listaProductos);
	                break;
	            case 2:
	            	//llamamos al metodo para que ense�e el stock de la tienda
	                mostrarProductos(listaProductos);
	                break;
	            case 3:
	            	//pasamos el nombre del producto quedeseamos eliminar y llamamos al metodo 
	                System.out.println("Introduce el nombre del producto que quieres eliminar:");
	                producto = eleccion.next();
	                eliminarProducto(producto, listaProductos);
	                break;
	            case 4:
	                break;  
	            default:
	                System.out.println("Opcion no valida");
	        	}
	    	}
	    }
	
	
	//con este metodo controlamos que el producto no este ya almacenado y si no lo esta agregara el nuevo producto al stock 
	public static void almacenarproducto(String nombreProducto, float precio, HashMap <String,Float> listaProductos){
	    if (listaProductos.containsKey(nombreProducto)) {
	        System.out.println("No se puede introducir el producto. El nombre del producto ya existe");
	    } else {
	        listaProductos.put(nombreProducto, precio);               
	    }
	}
	
	//comprobamos el stock que hay almacenado y que lo ense�e todo 
	public static void mostrarProductos(HashMap<String, Float> listaProductos) {
	    String clave;
	    Iterator<String> productos = listaProductos.keySet().iterator();
	    System.out.println("Stock:");
	    while(productos.hasNext()){
	        clave = productos.next();
	        System.out.println(clave + " - " + listaProductos.get(clave));
	    }        
	}
	
	//buscamos el producto con la key y si esta lo eliminamos con el remove
	public static void eliminarProducto(String producto, HashMap<String,Float> listaProductos) {
	    if (listaProductos.containsKey(producto)) {
	        listaProductos.remove(producto);
	    } else {
	        System.out.println("No hay ningun producto con ese nombre.");  
	    }       
	} 
	

}
