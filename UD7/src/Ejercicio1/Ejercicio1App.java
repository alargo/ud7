package Ejercicio1;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

public class Ejercicio1App {

	public static void main(String[] args) {
		//Autor: Alejandro Largo
		
		//Se pide el numero de alumnos
		
		Scanner obtenerNumero = new Scanner(System.in);
		System.out.println("Introduce el numero de alumnos ");
		int alumnos = obtenerNumero.nextInt();
		int contador = 0;
		
		//con el aarraylist se van a almacenar las notas, en este caso se han dado unas notas fijas pero pueden ir cambiando
		while(contador != alumnos) {
			ArrayList<Integer> lista = new ArrayList();
			
			lista.add(5);
			lista.add(6);
			lista.add(7);
			lista.add(8);
			
			int notamedia=0;
			
			//se hace un bulce para sacar la nota media de todas las notas que se tienen 
			for (Integer notas:lista) {
				notamedia = notamedia + notas;		
			}
			
			notamedia = notamedia / 4;
			
		//se hace otro bucle para mostar los alumnos y sus notas
		Hashtable<Integer, Integer> notas = new Hashtable<Integer, Integer>();
		
		notas.put(contador,notamedia);
	    
	    java.util.Enumeration claves = notas.keys();
	    while( claves.hasMoreElements() ) {
        Object clave = claves.nextElement();
        Object valor = notas.get(clave);
        System.out.println("Estudiante: "+clave.toString()+", Nota Media:  " +valor.toString());
        }
	    
	    contador++;
		}

	}

}
