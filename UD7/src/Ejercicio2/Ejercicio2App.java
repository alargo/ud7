package Ejercicio2;

import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio2App {

	public static void main(String[] args) {
		//Autor: Alejandro Largo
		Scanner obtenerNumero = new Scanner(System.in);
		System.out.println("Introduce el numero de productos: ");
		int numProductos = obtenerNumero.nextInt();
		int contador = 0;
		
		ArrayList<Integer> lista = new ArrayList();
		
		int PrecioProductos = 0;
		
		//se hace un bucle para comprobar que los productos no sean 0  y despues a�adir el precio de los productos 
		while(contador != numProductos) {
			System.out.println("Introduce el precio de los productos: ");
			PrecioProductos = obtenerNumero.nextInt();
			
			lista.add(PrecioProductos);
			
			contador++;
			
			
		}
		
		//se calcula el iva del precio final y despues el cambio que se da dependiendo de la cantidad con la que se pague
		double precioFinalProducto = Iva(PrecioProductos);
		
		System.out.println("Introduce la cantidad pagada: ");
		double cantidadPagada = obtenerNumero.nextDouble();
		
		double cambioDevolver = cantidadPagada - precioFinalProducto;
		
		System.out.println("--------------------------------- RECIBO -------------------------------------");
		System.out.println("Cantidad de productos " + numProductos);
		System.out.println("Precio de los Productos: " + PrecioProductos);
		System.out.println("Precio total " + precioFinalProducto);
		System.out.println("Cantidad Pagada: " + cantidadPagada);
		System.out.println("El cambio es: " + cambioDevolver);
		
		
	}
	
	//Se calcula el iva con dos condiciones que van a calcular un iva diferente dependiendo dle iva que se le pase por teclado
	public static double Iva(double precio) {

		double iva1 = 0.21;
		double iva2 = 0.04;
		
		double ivafinal = 0;
		
		Scanner obtenerNumero = new Scanner(System.in);
		System.out.println("Introduce el Iva del producto: ");
		double cantidadIva = obtenerNumero.nextDouble();
		
		if(cantidadIva == iva1) {
			ivafinal = precio * iva1;
			precio = precio + ivafinal;
			
			System.out.println("El precio del producto es: " + precio);
		}else {
			ivafinal = precio * iva2;
			precio = precio + ivafinal;
			System.out.println("El precio del producto es: " + precio);
			
		}
		
		return precio;
		
	}
	


}
